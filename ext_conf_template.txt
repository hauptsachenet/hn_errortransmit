# cat=basic/enable; type=string; label= Slack channel: Slack channel to send errors. Not send message if empty! default: #500er
sendToSlackChannel = #500er

# cat=basic/enable; type=string; label= Mail address: Mailaddress (kommalist) to send errors. Not send message if empty!
sendToMails =