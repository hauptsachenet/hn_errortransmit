<?php

namespace Hn\HnErrortransmit\Hooks;

/**
 * $GLOBALS['TYPO3_CONF_VARS']['SYS']['systemLogLevel'] must not be less than 4 (SYSLOG_SEVERITY_FATAL)
 */
use TYPO3\CMS\Core\SingletonInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class SysLogHook implements SingletonInterface {

    /**
     * @var \Hn\HnErrortransmit\Utility\MessageUtility
     */
    protected $messageUtility;
    /**
     *
     */
    public function __construct() {
        $this->messageUtility = GeneralUtility::makeInstance('Hn\\HnErrortransmit\\Utility\\MessageUtility');
    }


    /**
     * SYSLOG_SEVERITY_INFO = 0; SYSLOG_SEVERITY_NOTICE = 1; SYSLOG_SEVERITY_WARNING = 2; SYSLOG_SEVERITY_ERROR = 3; SYSLOG_SEVERITY_FATAL = 4;
     * @param $params
     * @param $ref
     */
    public function sendMessage(&$params, &$ref) {
        if($params['severity'] >= \TYPO3\CMS\Core\Utility\GeneralUtility::SYSLOG_SEVERITY_ERROR) {
            $this->messageUtility->send(strip_tags($params['msg']));
        }

    }

}