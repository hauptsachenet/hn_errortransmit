<?php

namespace Hn\HnErrortransmit\Utility;

use TYPO3\CMS\Core\SingletonInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class MessageUtility implements SingletonInterface {

    /**
     * @var array
     */
    protected $extConfig;

    /**
     * construct
     */
    public function __construct() {
        $this->extConfig = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['hn_errortransmit']);
    }

    /**
     * @param string $message
     */
    public function send($message) {
        // only 1 message for request
        if($GLOBALS['HN']['sendErrorMessage']) return false;

        if(GeneralUtility::getIndpEnv(HTTP_REFERER)) {
            $message .= ' HTTP_REFERER: ' . GeneralUtility::getIndpEnv(HTTP_REFERER).'.';
        }
        // send to slack
        $slackChannel = trim($this->extConfig['sendToSlackChannel']);

        if($slackChannel) {
            !self::slack($slackChannel, $message);
        }

        // send to mails
        $mails = GeneralUtility::trimExplode(',', $this->extConfig['sendToMails'], TRUE);
        if($mails) {
            $mail = GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Mail\\MailMessage');
            /** @var \TYPO3\CMS\Core\Mail\MailMessage $mail */
            $mail->setFrom(array('info@hauptsache.net'=>'hn_errortransmit'))
                ->setTo(array_shift($mails))
                ->setSubject('hn_errortransmit: '.GeneralUtility::getIndpEnv(TYPO3_HOST_ONLY))
                ->setBody($message,'text/html')
                ->send();
            foreach($mails as $mail) {
                $mail->addTo($mail);
            }
        }

        $GLOBALS['HN']['sendErrorMessage'] = TRUE;
    }


    /**
     * @param $message
     * @param $channel
     * @return mixed
     */
    protected function slack($channel, $message) {
        $message = preg_replace('/((https?:\/\/\S+)|(\S+\.(com|de|net)\S+))/', '<!$1>' , $message);

        $ch = curl_init('https://slack.com/api/chat.postMessage');
        $data = http_build_query([
            'token' => 'xoxp-3758704670-3758704676-8888128822-09ba49',
            'channel' => $channel, //'#channel',
            'text' => $message,
            'username' => 'hn_errortransmit: '.GeneralUtility::getIndpEnv(TYPO3_HOST_ONLY),
        ]);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        curl_close($ch);

        return $result;
    }
}