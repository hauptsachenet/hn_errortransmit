<?php
namespace Hn\HnErrortransmit\ErrorHandler;

use TYPO3\CMS\Core\SingletonInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class FatalErrorHandler implements SingletonInterface{
    /**
     * @var \Hn\HnErrortransmit\Utility\MessageUtility
     */
    protected $messageUtility;

    /**
     * construct
     */
    public function __construct() {
        $this->messageUtility = GeneralUtility::makeInstance('Hn\\HnErrortransmit\\Utility\\MessageUtility');
        register_shutdown_function(array($this, 'fatalErrorShutdownHandler'));
    }

    /**
     * fatals error
     */
    public function fatalErrorShutdownHandler() {
        $lastError = error_get_last();
        if ($lastError['type'] === E_ERROR) {
           $this->messageUtility->send('Fatal error: '.$lastError['message'].' in '.$lastError['file'].' on line '.$lastError['line'].'. Requested URL: '.GeneralUtility::getIndpEnv(TYPO3_REQUEST_URL));
        }
    }

}